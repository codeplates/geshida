<?php 

namespace App;

use Illuminate\Support\Str;
use DB;

// FIXME: use laravels default migrator through dependency injection. 
// stop reinventing za wheel. check if migrator->reset works as expected

class Migrator {

	public function run($path)
	{
	    
	   $files = $this->getMigrationFiles($path);

	   // $this->requireFiles($migrations = $this->pendingMigrations(
	   //    $files, $this->repository->getRan()
	   // ));
	   $this->runMigrations($files);
	}

	public function getMigrationFiles($path)
	{
		$paths = glob(base_path($path.'/*_*.php'));
		$files = [];

		foreach ($paths as $path) {
			$name = str_replace('.php', '', basename($path));
			$files[$name] = $path;
		}
		return $files;
	}

	public function runMigrations($files, $depModuleId = null)
	{
		$batch = 1 + DB::table('migrations')->max('batch');
		$migrations = [];

		foreach ($files as $name => $file) {
			$instance = $this->resolve($name, $file);
			$instance->up();

			$migrations[] = [
				'migration' => $name, 
				'batch' => $batch
			];
		}

		DB::table('migrations')->insert($migrations);
	}

	public function resolve($name, $file)
	{
	   require_once($file);
	   $class = Str::studly(implode('_', array_slice(explode('_', $name), 4)));
	   return new $class;
	}


}