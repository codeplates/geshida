<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use App\Facades\Settings;
use App\Subsystem;

class SubsystemServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $subsystems = Settings::get('active_subsystems', []);
        foreach ($subsystems as $subsystemName) {
            $this->loadSubsystemRoutes($subsystemName);
            $subsystem = Subsystem::resolve($subsystemName);
            $subsystem->boot();
        }
    }

    // FIXME: move this to subsystem's own provider to improve customizability
    protected function loadSubsystemRoutes($subsystemName)
    {
        $routefile = base_path("cms/subsystems/{$subsystemName}/routes/web.php");
        $namespace = "Subsystem\\$subsystemName\\App\\Http\\Controllers";
        if (file_exists($routefile)) {
            Route::namespace($namespace)->group(function() use ($routefile) {
                $this->loadRoutesFrom($routefile);            
            });
        }
    }
}
