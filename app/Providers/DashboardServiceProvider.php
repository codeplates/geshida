<?php

namespace App\Providers;

use App\Crud;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class DashboardServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // FIXME: create view composer for this
        View::composer('admin.partials.menu', function ($view) {
            $cruds = [];
            foreach (Crud::getModels() as $crud) {
                if ($crud['options']['show_in_dashboard_menu']) {
                    $cruds[] = $crud;
                }
            }

            $view->with('cruds', $cruds);
        });
    }
}
