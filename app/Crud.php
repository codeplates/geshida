<?php 

namespace App;

use Illuminate\Support\Str;

class Crud {

	protected static $models = [];

	public static function register($name, $labels, $options)
	{
		// TODO: handle errors/issues better. maybe use exceptions 
		if (isset(self::$models[$name])) 
			die("Crud Already registered");
		
		self::$models[$name] = [
			'labels'  => self::fillLabels($labels),
			'options' => self::fillOptions($options)
		];
	}

	protected static function fillLabels($labels)
	{
		return [
			'name' 			=> $labels['name'],
			'name_plural'	=> $labels['name_plural'] ?? Str::plural($labels['name'])
		];
	}

	protected static function fillOptions($options)
	{
		$defaultOptions = [
			'show_in_dashboard_menu' => true
		];
		return array_merge($defaultOptions, $options);
	}

	public static function getModels() {
		return self::$models;
	}

}