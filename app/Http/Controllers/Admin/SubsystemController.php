<?php

namespace App\Http\Controllers\Admin;

use App\Subsystem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Settings;

class SubsystemController extends Controller
{
    //
	public function index()
	{
		$subsystems = Subsystem::getFolderSubsystems();
		return view('admin.subsystems.index', compact('subsystems'));
	}

	public function install(Request $request)	
	{
		$subsystem = Subsystem::resolve($request->subsystem);
		$subsystem->install();

		Settings::append('installed_subsystems', $subsystem->getName());
		return redirect('/admin/subsystems');
	}

	public function enable(Request $request)
	{
		$subsystem = Subsystem::resolve($request->subsystem);
		$subsystem->enable();

		Settings::append('active_subsystems', $subsystem->getName());
		return redirect('/admin/subsystems');
	}

	public function disable(Request $request)
	{
		$subsystem = Subsystem::resolve($request->subsystem);
		$subsystem->disable();

		Settings::remove('active_subsystems', $subsystem->getName());
		return redirect('/admin/subsystems');
	}
}
