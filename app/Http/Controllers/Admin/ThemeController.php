<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Facades\Settings;
use Illuminate\Support\Facades\Storage;

class ThemeController extends Controller
{
   /**
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct()
   {
       $this->middleware('auth');
   }

   /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
   public function index()
   {
   	$current_theme = Settings::get('current_theme', 'default');

   	$directories = Storage::disk('themes')->directories();
   	$themes = [];
   	foreach ($directories as $theme_dir) {
   		$theme_json = $theme_dir."/theme.json";
   		if(Storage::disk('themes')->exists($theme_json)) {
   			$json = Storage::disk('themes')->get($theme_json);
   			if ($theme_dir == $current_theme)
   				$themes = [$theme_dir => json_decode($json)] + $themes;
   			else
   				$themes[$theme_dir] = json_decode($json);
   		} else {
   			// TODO: message invalid theme detected
   		}	
   	}
      return view('admin.themes.index', compact('themes', 'current_theme'));
   }

   public function set(Request $request)
   {
   	$theme = $request->theme;
   	if(Storage::disk('themes')->exists($theme)) {
   		Settings::set('current_theme', $theme);
   	} else {
   		dd("Unknown Theme");
   	}
   	return redirect('/admin/themes');
   }
}
