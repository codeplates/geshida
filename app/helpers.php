<?php 

if (! function_exists('theme_view')) {
	function theme_view($view = null, $data = [], $mergeData = [])
	{
		// get current theme from settings
	   $theme = Settings::get('current_theme', 'default');

	   return view("geshida::${theme}.${view}", $data, $mergeData);
	}
}