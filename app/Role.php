<?php

namespace App;

use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{
	public static $permissions = [
		'view dashboard',
		'create user',
	];

	protected $fillable = ['name', 'description', 'guard_name'];
}
