<?php 

Route::prefix('page')->group(function () {
	
	Route::get('/{slug}', 'PageController@view_page');

});