<?php

use App\Subsystem;
use App\Crud;
/**
 * 
 */
class PageSubsystem extends Subsystem
{
	
	protected $prefferedRoute = "page";

	protected $name = "Page";
	/** 
	* TODO 
	* - add crud models 
	* - option add to menu 
	*
	*/ 

	public function boot()
	{
		$pageLabels =  [
			'name' => 'Page'
		];

		$pageOptions = [
			'show_in_dashboard_menu' => true
		];

		Crud::register('pages', $pageLabels, $pageOptions);
	}

}