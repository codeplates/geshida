<?php

use App\User;
Use App\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillDefaultUser extends Migration
{

    public function __construct()
    {
        $this->users = [
            [
                'name' => 'Joshua Kisb',
                'email' => 'joshuakisb@gmail.com',
                'password' => Hash::make('pass@gesh'),
            ]
        ];
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
           
            foreach (Role::$permissions as $permission) {
                Permission::create(['name' => $permission]);
            }

            foreach ($this->users as $user) {
                //$role = Role::where('name', $user['role'])->first();
                
                $newUser = new User();
                $newUser->fill($user);
                //$newUser->role()->associate($role);
                $newUser->save();
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            foreach ($this->users as $user) {
                User::where('email', $user['email'])->forceDelete();
            }

            foreach (Role::$permissions as $permission) {
                Permission::where('name', $permission)->delete();
            }
        });
    }
}
