<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Auth::routes();

Route::get('/admin', 'Admin\IndexController@index')->name('admin');
Route::get('/admin/themes', 'Admin\ThemeController@index');
Route::post('/admin/themes/set', 'Admin\ThemeController@set');

Route::get('/admin/subsystems', 'Admin\SubsystemController@index');
Route::post('/admin/subsystems/{subsystem}/install', 'Admin\SubsystemController@install');
Route::post('/admin/subsystems/{subsystem}/enable', 'Admin\SubsystemController@enable');
Route::post('/admin/subsystems/{subsystem}/disable', 'Admin\SubsystemController@disable');

Route::resource('users', 'Admin\UserController');
Route::resource('roles', 'Admin\RoleController');
