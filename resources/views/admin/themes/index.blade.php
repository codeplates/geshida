@extends("admin.layouts.admin")

@section("content")


<h1 class="title">Available Themes</h1>

<div class="theme-list">
	@foreach($themes as $key => $theme)
		<div class="theme box {{ ($key == $current_theme) ? "current": '' }}">
			<div class="thumbnail"></div>
			<div class="title is-3">{{ $theme->name }}</div>
			<div class="actions">
				@if ($key != $current_theme)
				<form action="{{ url('/admin/themes/set') }}" method="post">
					{{ csrf_field() }}
					<input type="hidden" name="theme" value="{{ $key }}">
					<button class="button is-primary mx-auto">Activate</button>
				</form>
				@endif
			</div>
		</div>
	@endforeach
</div>
@endsection