@extends("admin.layouts.admin")

@section("content")


<h1 class="title">Subsytems</h1>

<div class="subsystem-list">
	@foreach($subsystems as $subsystem)
		<div class="columns" style="border: 1px solid #c9c9c9">
			<div class="column is-2">{{ $subsystem->name }}</div>
			<div class="column is-6">{{ $subsystem->description }}</div>
			<div class="column is-3">
				@if (!$subsystem->installed)
				<form style="display: inline;" action="{{ url('/admin/subsystems/'.$subsystem->id.'/install') }}" method="POST">
					{{ csrf_field() }}
					<button type="submit" class="button is-secondary">
						Install
					</button>
				</form>
				@elseif (!$subsystem->active)
				<form style="display: inline;" action="{{ url('/admin/subsystems/'.$subsystem->id.'/enable') }}" method="POST">
					{{ csrf_field() }}
					<button type="submit" class="button is-primary">
						Enable
					</button>
				</form>

				<form style="display: inline;" action="{{ url('/admin/subsystems/'.$subsystem->id.'/uninstall') }}" method="POST">
					{{ csrf_field() }}
					<button type="submit" class="button is-danger">
						Uninstall
					</button>
				</form>
				@else
				<form style="display: inline;" action="{{ url('/admin/subsystems/'.$subsystem->id.'/disable') }}" method="POST">
					{{ csrf_field() }}
					<button type="submit" class="button is-secondary">
						Disable
					</button>
				</form>

				<form style="display: inline;" action="{{ url('/admin/subsystems/'.$subsystem->id.'/uninstall') }}" method="POST">
					{{ csrf_field() }}
					<button type="submit" class="button is-danger">
						Uninstall
					</button>
				</form>

				@endif
			</div>
		</div>
	@endforeach
</div>

@endsection