<aside id="sidebar" class="menu column is-2">
	<div class="container">
		<p class="menu-label">
			General
		</p>
		<ul class="menu-list">
			<li><a href="{{ url('/admin') }}">Dashboard</a></li>
			@foreach($cruds as $crud)
			<li><a href="#">{{ $crud['labels']['name_plural'] }}</a></li>
			@endforeach
		</ul>
		<p class="menu-label">
			Administration
		</p>
		<ul class="menu-list">
			<li>
				<a>Users</a>
				<ul>
					<li><a href="{{ route('users.index') }}">Users</a></li>
					<li><a href="{{ route('roles.index') }}">Roles</a></li>
				</ul>
			</li>
			<li><a>Menus</a></li>
			<li><a class="is-active" href="{{ url('/admin/themes') }}">Themes</a></li>
			<li>
				<a class="" href="{{ url('/admin/subsystems') }}">Subsystems</a>
				<ul>
					<li><a>Routes</a></li>
					
				</ul>
			</li>
			
		</ul>
	</div>

</aside>