@extends("admin.layouts.admin")

@section("content")

<h1 class="title">Create Role</h1>

<form action="{{ route('roles.store') }}" method="post">
	{{ csrf_field() }}

	@include('admin.roles.components.form')

	<div class="field is-grouped">
		<div class="control">
			<button class="button is-link">Submit</button>
		</div>
		<div class="control">
			<button class="button is-link is-light">Cancel</button>
		</div>
	</div>
</form>


@endsection