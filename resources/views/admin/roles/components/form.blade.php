<div class="field">
	<label class="label">Name</label>
	<div class="control">
		<input class="input" type="text" name="name" placeholder="Role name" value="{{ old('name', $role->name) }}">
	</div>
</div>

<div class="field">
	<label class="label">Description</label>
	<div class="control">
		<textarea class="textarea" name="description" placeholder="Role description">{{ old('description', $role->description) }}</textarea>
	</div>
</div>

<h2 class="subtitle">Permissions</h2>

<div class="columns">
	@php 
	$oldperms = is_array(old('permissions')) ? old('permissions') : $role->getPermissionNames()->toArray();
	@endphp
	@foreach($permissions as $permission)
	<div class="field column is-4">
		<div class="control">
			<label class="checkbox">
				<input type="checkbox" name="permissions[]" value="{{ $permission }}" {{ in_array($permission, $oldperms)?'checked':'' }}>
				{{ $permission }}
			</label>
		</div>
	</div>
	@endforeach
</div>