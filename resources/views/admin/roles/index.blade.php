@extends("admin.layouts.admin")

@section("content")


<h1 class="title">Roles</h1>
<a class="button is-pulled-right" href="{{ route('roles.create') }}">Create Role</a>

<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
	<thead>
		<tr>
			<td></td>
			<td>Name</td>
			<td>Description</td>
			<td>Actions</td>
		</tr>
	</thead>
	<tbody>
		@foreach($roles as $role)
		<tr>
			<td></td>
			<td>{{ $role->name }}</td>
			<td>{{ $role->description }}</td>
			<td>
				{{-- @can('update', $role) --}}
				<a href="{{ route('roles.edit', [$role->id]) }}" class="button is-primary">
					<i class="fa fa-pencil"></i>
				</a>
				{{-- @endcan --}}
				{{-- @can('delete', $role) --}}
				<form style="display: inline;" action="{{ route('roles.destroy', [$role->id]) }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
					<button type="submit" class="button is-danger">
						<i class="fa fa-trash"></i>
					</button>
				</form>
				{{-- @endcan --}}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>



@endsection