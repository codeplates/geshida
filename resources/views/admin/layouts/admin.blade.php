<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Dashboard</title>
	<link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="{{ mix('/css/admin.css') }}">
</head>
<body>
		<nav class="navbar" role="navigation" aria-label="main navigation">
			<div class="container">
				<div class="navbar-brand">
					<a class="navbar-item" href="#">
						<h2>Geshida</h2>
					</a>

					<a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
						<span aria-hidden="true"></span>
						<span aria-hidden="true"></span>
						<span aria-hidden="true"></span>
					</a>
				</div>

				<div id="navbarBasicExample" class="navbar-menu">
					<div class="navbar-end">
						<div class="navbar-item">
							Joshua
						</div>
					</div>
				</div>
			</div>
		</nav>

	<div id="wrapper" class="columns is-mobile">

		@include("admin.partials.menu")

		<div id="main" class="column section">
			<div class="container">
				@yield("content")
			</div>
		</div>

	</div>
</body>
</html>