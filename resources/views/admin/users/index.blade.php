@extends("admin.layouts.admin")

@section("content")


<h1 class="title">Users</h1>
<a class="button is-pulled-right" href="{{ route('users.create') }}">Create User</a>


<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
	<thead>
		<tr>
			<td></td>
			<td>Name</td>
			<td>Role</td>
			<td>Email</td>
			<td>Actions</td>
		</tr>
	</thead>
	<tbody>
		@foreach($users as $user)
		<tr>
			<td></td>
			<td>{{ $user->name }}</td>
			<td>{{ $user->role->name ?? '' }}</td>
			<td>{{ $user->email }}</td>
			<td>
				{{-- @can('update', $user) --}}
				<a href="{{ route('users.edit', [$user->id]) }}" class="button is-primary">
					<i class="fa fa-pencil"></i>
				</a>
				{{-- @endcan --}}
				{{-- @can('delete', $user) --}}
				<form style="display: inline;" action="{{ route('users.destroy', [$user->id]) }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
					<button type="submit" class="button is-danger">
						<i class="fa fa-trash"></i>
					</button>
				</form>
				{{-- @endcan --}}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

@endsection