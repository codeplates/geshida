<div class="field">
	<label class="label">Role</label>
	<div class="select">
		<select name="role">
			<option value="">Select role</option>
			@foreach($roles as $role)
			<option value="{{ $role->id }}" {{ (old('role', $user->roles->first()->id ?? '') == $role->id ) ? 'selected' : '' }}>{{ $role->name }}</option>
			@endforeach
		</select>
	</div>
</div>

<div class="field">
	<label class="label">Name</label>
	<div class="control">
		<input class="input" type="text" name="name" placeholder="Full Name" value="{{ old('name', $user->name) }}">
	</div>
</div>

<div class="field">
	<label class="label">Email</label>
	<div class="control">
		<input class="input" type="email" name="email" placeholder="Email" value="{{ old('name', $user->email) }}">
	</div>
</div>

<div class="field">
	<label class="label">Password</label>
	<div class="control">
		<input class="input" type="password" name="password">
	</div>
</div>