@extends("admin.layouts.admin")

@section("content")

<h1 class="title">Edit User</h1>

<form action="{{ route('users.update', [$user->id]) }}" method="post">
	{{ csrf_field() }}
	{{ method_field('PATCH') }}

	@include('admin.users.components.form')

	<div class="field is-grouped">
		<div class="control">
			<button class="button is-link">Submit</button>
		</div>
		<div class="control">
			<button class="button is-link is-light">Cancel</button>
		</div>
	</div>
</form>


@endsection